/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3_1;

/**
 *
 * @author natta
 */
public class Lab3_1 {

     public static void main(String[] args) {
        int x1 = 4;
        System.out.println("Output: " + mySqrt(x1));
        
        int x2 = 8;
        System.out.println("Output: " + mySqrt(x2));
    }
    

    private static int mySqrt(int x) {
        if (x <= 1) {
            return x; 
        }
        
        int left = 1; 
        int right = x; 
        int mid =0;
        
        while (left <= right) {
            mid = left + (right - left) / 2; 
            
            if (mid * mid == x) {
                return mid;
            } else if (mid * mid < x) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        
        return right ;
    }
}
